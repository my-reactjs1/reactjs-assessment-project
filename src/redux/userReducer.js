import { createSlice } from '@reduxjs/toolkit';
import { users } from '../data';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    value: null,
    isAuthenticated: false,
  },

  reducers: {
    setUser: (state, action) => {
      state.value = action.payload;
      localStorage.setItem('active_username', action.payload.username);
      state.isAuthenticated = true;
    },

    logout: (state) => {
      state.value = null;
      state.isAuthenticated = false;
      localStorage.removeItem('active_username');
    },

    checkAuth: (state) => {
      const activeUser = localStorage.getItem('active_username');
      if (activeUser) {
        const user = users.find((value) => {
          return value.username === activeUser;
        });

        if (!user) {
          state.value = null;
          state.isAuthenticated = false;
          return;
        }

        state.value = user;
        state.isAuthenticated = true;
      } else {
        state.value = null;
        state.isAuthenticated = false;
      }
    },
  },
});

export const { setUser, logout, checkAuth } = userSlice.actions;

export default userSlice.reducer;
