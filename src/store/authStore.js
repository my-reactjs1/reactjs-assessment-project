import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../redux/userReducer';

export const authStore = configureStore({
  reducer: {
    auth: userReducer,
  },
});
