import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Outlet, Route, Routes } from 'react-router-dom';
import ProtectedLayout from './component/layout/ProtectedLayout';
import UnprotectedLayout from './component/layout/UnprotectedLayout';
import NotFound from './pages/not-found/NotFound';
import NewPost from './pages/protected/post/component/NewPost';
import Post from './pages/protected/post/Post';
import DetailPost from './pages/protected/post/{id}/DetailPost';
import AboutMe from './pages/unprotected/about-me/AboutMe';
import PublicDetailPost from './pages/unprotected/home/component/PublicDetailPost';
import Home from './pages/unprotected/home/Home';
import Login from './pages/unprotected/login/Login';
import { checkAuth } from './redux/userReducer';

App.propTypes = {};

function App(props) {
  const dispatch = useDispatch();

  // check authentication on first loading
  useEffect(() => {
    dispatch(checkAuth());
  }, []);

  return (
    <Routes>
      <Route path='/' element={<UnprotectedLayout />}>
        <Route index element={<Home />}></Route>
        <Route path='post' element={<Outlet />}>
          <Route index element={<Home />} />
          <Route path=':id' element={<PublicDetailPost />} />
        </Route>
        <Route path='login' element={<Login />}></Route>
        <Route path='about-me' element={<AboutMe />}></Route>
      </Route>

      <Route path='/' element={<ProtectedLayout />}>
        <Route path='posts' element={<Outlet />}>
          <Route index element={<Post />} />
          <Route path='new' element={<NewPost />} />
          <Route path=':id' element={<DetailPost />} />
        </Route>
        <Route path='about' element={<AboutMe />}></Route>
      </Route>

      <Route path='*' element={<NotFound />}></Route>
    </Routes>
  );
}

export default App;
