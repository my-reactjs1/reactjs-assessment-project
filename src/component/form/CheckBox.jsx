import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBox, faCheck, faSquare } from '@fortawesome/free-solid-svg-icons';

CheckBox.propTypes = {
  checked: PropTypes.bool,
  value: PropTypes.any,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  formArgs: PropTypes.object,
  id: PropTypes.string,
  children: PropTypes.any,
};

CheckBox.defaultProps = {
  checked: false,
  value: '',
  disabled: false,
  onChange: (e) => {},
  formArgs: {},
  children: '',
};

function CheckBox(props) {
  const { checked, value, disabled, onChange, id, children, formArgs } = props;

  const [isChecked, setIsChecked] = useState(checked);

  useEffect(() => {
    onChange(isChecked);
  }, [isChecked]);

  return (
    <button
      type='button'
      onClick={() => {
        setIsChecked(!isChecked);
      }}
      className=' inline-flex items-center gap-2 group select-none '
    >
      <label className='bg-secondary-dark border-teal-700 border h-7 w-7 cursor-pointer grid '>
        <FontAwesomeIcon
          icon={faCheck}
          className={'m-auto ' + (!isChecked && 'opacity-0')}
        />
      </label>

      <label className='cursor-pointer'>{children}</label>

      <input
        type={'checkbox'}
        className={'hidden'}
        value={value}
        disabled={disabled}
        id={id}
        checked={isChecked}
        onChange={(e) => {
          setIsChecked(e.target.checked);
        }}
        {...formArgs}
      />
    </button>
  );
}

export default CheckBox;
