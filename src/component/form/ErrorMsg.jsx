import React from 'react';
import PropTypes from 'prop-types';

ErrorMsg.propTypes = {
	errors: PropTypes.string,
	reqErrors: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

function ErrorMsg(props) {
	const { errors, reqErrors } = props;
	return (
		<div
			className={
				'flex flex-col text-right text-red-400 ' +
				(errors && reqErrors ? 'gap-1' : '')
			}
		>
			<span className=' first-letter:capitalize'>
				{errors ? errors.message : null}
			</span>
			<span className='flex flex-col first-letter:capitalize'>{reqErrors}</span>
		</div>
	);
}

export default ErrorMsg;
