import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnimatePresence, motion } from 'framer-motion';
import PropTypes from 'prop-types';
import useKey from '../../hooks/useKey';

OkCancelWarning.propTypes = {
  visible: PropTypes.bool,
  setVisible: PropTypes.func,
  title: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  onDelete: PropTypes.func,
  isLoading: PropTypes.bool,
  isError: PropTypes.bool,
  isSuccess: PropTypes.bool,
};

OkCancelWarning.defaultProps = {
  setVisible: () => {},
  onDelete: () => {},
};

function OkCancelWarning(props) {
  const {
    visible,
    setVisible,
    children,
    title,
    onDelete,

  } = props;

  function toggle() {
    setVisible(!visible);
  }

  useKey('Escape', () => {
    if (visible) {
      setVisible(false);
    }
  });

  return (
    <AnimatePresence exitBeforeEnter>
      {visible && (
        <motion.div
          key='content'
          initial='collapsed'
          animate='open'
          exit='collapsed'
          className='fixed top-0 left-0 z-50 inline-flex items-center justify-center w-screen h-screen p-5 bg-gray-900 lef-0 backdrop-blur bg-opacity-5 '
          variants={{
            open: { opacity: 1 },
            collapsed: { opacity: 0 },
          }}
          transition={{ duration: 0.1 }}
        >
          <div className='w-full md:w-3/4 lg:w-1/2 '>
            <div className='flex flex-col gap-2 p-5 bg-primary-dark shadow-lg '>
              <div className='inline-flex items-center py-2 gap-2 font-bold text-teal-500 '>
                <FontAwesomeIcon
                  icon={faInfoCircle}
                />
                {title}
              </div>
              <hr />
              <div>{children}</div>
              <div className='flex flex-row justify-end gap-1'>
                <button type='button' onClick={toggle} className='w-20 btn-primary'>
                  Cancel
                </button>
                <button type='button' onClick={onDelete} className='w-20 btn-danger'>
                  delete
                </button>
              </div>
            </div>
          </div>
        </motion.div>
      )}
    </AnimatePresence>
  );
}

export default OkCancelWarning;
