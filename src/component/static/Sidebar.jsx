import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link, NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAddressCard,
  faFileLines,
  faHome,
  faLock,
  faRightFromBracket,
} from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../redux/userReducer';

Sidebar.propTypes = {};

function Sidebar(props) {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  return (
    <div className=' p-5 space-y-5'>
      <Link to='/'>
        <h5 className='text-center'>Simple Post App</h5>
      </Link>
      <hr />
      <div className='flex flex-col gap-1 '>
        <NavLink
          className={(navData) => {
            return (
              'nav-item justify-start w-full  ' +
              (navData.isActive && 'nav-item-active')
            );
          }}
          to='/posts'
        >
          <FontAwesomeIcon className='w-10' icon={faFileLines} />
          POSTS
        </NavLink>

        <NavLink
          className={(navData) => {
            return (
              'nav-item justify-start w-full  ' +
              (navData.isActive && 'nav-item-active')
            );
          }}
          to='/about'
        >
          <FontAwesomeIcon className='w-10' icon={faAddressCard} />
          ABOUT
        </NavLink>

        <NavLink
          className={(navData) => {
            return (
              'nav-item justify-start w-full  ' +
              (navData.isActive && 'nav-item-active')
            );
          }}
          to=''
          onClick={() => dispatch(logout())}
        >
          <FontAwesomeIcon className='w-10' icon={faRightFromBracket} />
          LOGOUT
        </NavLink>
      </div>
    </div>
  );
}

export default Sidebar;
