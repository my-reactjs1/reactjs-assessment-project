import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnimatePresence, motion } from 'framer-motion';

import {
  faAddressCard,
  faBars,
  faHome,
  faLock,
  faSquare,
  faTable,
} from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import useWindowSize from '../../hooks/useWindowsSize';
import useOutsideClick from '../../hooks/useOutsideClick';

Header.propTypes = {};

function Header(props) {
  const auth = useSelector((state) => state.auth);

  const [isTopBarOpen, setIsTopBarOpen] = useState(window.innerWidth > 480);
  const toggleTopBar = () => setIsTopBarOpen(!isTopBarOpen);

  // handle outside click, if topbar navigation item is still open in mobile mode, it will outomatically close if user clicked outside te header container
  const sidebarContainer = useRef();
  useOutsideClick(sidebarContainer, () => {
    if (window.innerWidth < 640 && isTopBarOpen) {
      setIsTopBarOpen(false);
    }
  });

  // tracking windows resize, forcing header to open its naviagation item on medium and large screen
  const size = useWindowSize();
  useEffect(() => {
    if (size[0] >= 480 && !isTopBarOpen) {
      setIsTopBarOpen(true);
    }
  }, [size]);

  return (
    <div
      ref={sidebarContainer}
      className='flex md:flex-row flex-col justify-between  min-h-20 lg:items-center  py-5 md:py-5 '
    >
      <div className='flex flex-row justify-between'>
        <div className=''>
          <h4 className=' '>Simple Post App</h4>
        </div>
        <div className='block md:hidden'>
          <button
            onClick={toggleTopBar}
            className='btn-secondary border-none w-10'
          >
            <FontAwesomeIcon icon={faBars} />
          </button>
        </div>
      </div>

      <AnimatePresence initial={false}>
        {isTopBarOpen && (
          <motion.section
            key='content'
            initial='collapsed'
            animate='open'
            className='flex md:flex-row flex-col gap-2'
            exit='collapsed'
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 },
            }}
            transition={{ duration: 0.2, ease: [0.04, 0.62, 0.23, 0.98] }}
          >
            <NavLink
              className={(navData) => {
                return (
                  'nav-item justify-start md:justify-center ' +
                  (navData.isActive && 'nav-item-active')
                );
              }}
              to='/'
            >
              <FontAwesomeIcon className='w-10 md:w-auto' icon={faHome} />
              HOME
            </NavLink>
            <NavLink
              className={(navData) => {
                return (
                  'nav-item justify-start md:justify-center ' +
                  (navData.isActive && 'nav-item-active')
                );
              }}
              to='/about-me'
            >
              <FontAwesomeIcon
                className='w-10 md:w-auto'
                icon={faAddressCard}
              />
              ABOUT ME
            </NavLink>
            <NavLink
              className={(navData) => {
                return (
                  'nav-item justify-start md:justify-center ' +
                  (navData.isActive && 'nav-item-active')
                );
              }}
              to={auth.isAuthenticated ? '/posts' : '/login'}
            >
              {auth.isAuthenticated ? (
                <span className='inline-flex items-center gap-2'>
                  <FontAwesomeIcon className='w-10 md:w-auto' icon={faTable} />
                  MY POSTS
                </span>
              ) : (
                <span className='inline-flex items-center gap-2'>
                  <FontAwesomeIcon className='w-10 md:w-auto' icon={faLock} />
                  LOGIN
                </span>
              )}
            </NavLink>
          </motion.section>
        )}
      </AnimatePresence>
    </div>
  );
}

export default Header;
