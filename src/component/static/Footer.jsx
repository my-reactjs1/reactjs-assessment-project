import React from 'react';
import PropTypes from 'prop-types';

Footer.propTypes = {};

function Footer(props) {
  return (
    <ul>
      <li>Alidin</li>
      <li>2022</li>
      <li>ReactJS Assestment Project</li>
    </ul>
  );
}

export default Footer;
