import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Sidebar from '../static/Sidebar';
import { Outlet, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { checkAuth } from '../../redux/userReducer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import useWindowSize from '../../hooks/useWindowsSize';
import useOutsideClick from '../../hooks/useOutsideClick';

ProtectedLayout.propTypes = {};

function ProtectedLayout(props) {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  const [isSidebarOpen, setIsSidebarOpen] = useState(window.innerWidth >= 720);
  const toggle = () => setIsSidebarOpen(!isSidebarOpen);

  const navigate = useNavigate();

  const sidebarContainer = useRef();
  useOutsideClick(sidebarContainer, () => {
    if (window.innerWidth < 640 && isSidebarOpen) {
      setIsSidebarOpen(false);
    }
  });

  useEffect(() => {
    if (!auth.isAuthenticated && !localStorage.getItem('active_username')) {
      navigate('/');
    }
  }, [auth]);

  const size = useWindowSize();

  useEffect(() => {
    if (size[0] >= 720 && !isSidebarOpen) {
      setIsSidebarOpen(true);
    }
  }, [size]);

  return (
    <div className=''>
      <div
        ref={sidebarContainer}
        className={
          'bg-secondary-dark w-60 lg:w-72 min-h-screen fixed transition-all duration-300 shadow-xl shadow-black ' +
          (isSidebarOpen ? '  ' : 'lg:-ml-72 -ml-60')
        }
      >
        <Sidebar />
        <button
          onClick={toggle}
          className={
            'fixed rounded-l-none top-5 btn-secondary border-none bg-secondary-dark h-12 lg:hidden  ' +
            (isSidebarOpen ? 'left-60  ' : 'left-0')
          }
        >
          <FontAwesomeIcon icon={faBars} />
        </button>
      </div>

      <div
        className={
          ' p-5 pl-10  md:p-10 lg:p-20 transition-all duration-300 ' +
          (isSidebarOpen ? 'lg:ml-72 md:ml-60 ml-0' : ' lg:ml-72 ml-0 ')
        }
      >
        <Outlet />
      </div>
    </div>
  );
}

export default ProtectedLayout;
