import React from 'react';
import PropTypes from 'prop-types';
import { Outlet } from 'react-router-dom';
import Header from '../static/Header';
import Footer from '../static/Footer';

UnprotectedLayout.propTypes = {};

function UnprotectedLayout(props) {
  return (
    <div className='flex flex-col min-h-full w-full items-center gap-10'>
      <div className='container-auto bg-secondary-dark w-full '>
        <Header />
      </div>
      <div className='container-auto flex-grow w-full '>
        <Outlet />
      </div>
      <footer className='container-auto w-full bg-secondary-dark py-5'>
        <Footer />
      </footer>
    </div>
  );
}

export default UnprotectedLayout;
