import * as uuid from 'uuid';

export function getAllPost() {
  const posts = localStorage.getItem('posts');
  if (!posts) {
    return [];
  }
  return JSON.parse(posts);
}

// search public posts
export function searchPublicPosts(search) {
  const posts = getAllPost().filter((post) => {
    return (
      post.isPublic &&
      (post.title.includes(search) || post.content.includes(search))
    );
  });

  if (!posts) {
    return [];
  }

  return posts;
}

// search post that belongs to active user
export function searchUserPosts(userId, search) {
  const posts = getAllPost().filter((post) => {
    return (
      post.writerId === userId &&
      (post.title.includes(search) || post.content.includes(search))
    );
  });

  if (!posts) {
    return [];
  }

  return posts;
}

export function createNewPost(form) {
  const newPost = {
    id: uuid.v1(),
    updatedAt: new Date().toISOString(),
    ...form,
  };

  localStorage.setItem('posts', JSON.stringify([...getAllPost(), newPost]));
  return newPost;
}

// toggling post mode (isPublic)
export function togglePublicOfPostById(userId, id) {
  const updated = getAllPost().map((value) => {
    if (value.id === id && value.writerId === userId) {
      return {
        ...value,
        isPublic: !value.isPublic,
        updatedAt: new Date().toISOString(),
      };
    }
    return value;
  });

  localStorage.setItem('posts', JSON.stringify(updated));
}

// get all public post from any user
export function getPublicPostById(id) {
  return getAllPost().find((value) => {
    return value.id === id && value.isPublic;
  });
}

// get post that belongs to active user
export function getCurrenUserPost(userId) {
  return getAllPost().filter((value) => value.writerId === userId);
}

export function getCurrenUserPostById(userId, id) {
  return getCurrenUserPost(userId, id).filter((value) => value.id === id)[0];
}

//only update post that belongs to active user
export function updateCurrenUserPostById(userId, id, form) {
  const updated = getAllPost().map((value) => {
    if (value.id === id && value.writerId === userId) {
      return {
        ...value,
        ...form,
        updatedAt: new Date().toISOString(),
      };
    }
    return value;
  });

  localStorage.setItem('posts', JSON.stringify(updated));
}

// only delete post that belongs to active user
export function deletePostById(userId, id) {
  const newPostList = getAllPost().filter((post) => {
    return post.id !== id || post.writerId !== userId;
  });

  localStorage.setItem('posts', JSON.stringify(newPostList));
}
