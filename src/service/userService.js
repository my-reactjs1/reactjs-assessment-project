import { users } from '../data';

export function login(form) {
  const user = users.find((value) => {
    return value.username === form.username;
  });

  if (!user) {
    return {
      data: null,
      errors: {
        username: 'Username not found',
      },
    };
  }

  if (user.password !== form.password) {
    return {
      data: null,
      errors: {
        password: 'Password does not match',
      },
    };
  }

  return {
    data: user,
    errors: null,
  };
}
