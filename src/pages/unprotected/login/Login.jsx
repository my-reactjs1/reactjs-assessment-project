import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import loginImg from '../../../assets/images/login.png';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import ErrorMsg from '../../../component/form/ErrorMsg';
import { login as userLogin } from '../../../service/userService';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { setUser } from '../../../redux/userReducer';

// form validation schema
const loginSchema = yup
  .object()
  .shape({
    username: yup.string().required('Username is Required'),
    password: yup.string().required('Password is Required'),
  })
  .required();

Login.propTypes = {};
Login.defaultProps = {};

function Login(props) {
  const [reqError, setReqError] = useState();
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // check the authentication, if not authenticated stay, if not : redirect to my post page
  useEffect(() => {
    if (auth.isAuthenticated) {
      navigate('/posts');
    }
  }, [auth]);

  // initializing form validation
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(loginSchema),
    mode: 'onSubmit',
  });

  // handle user login
  function login(formData, e) {
    setReqError(null);
    const { data, errors: reqErrors } = userLogin(formData);

    if (reqErrors) {
      setReqError(reqErrors);
      return;
    }

    dispatch(setUser(data));
  }

  return (
    <div className='flex flex-row bg-secondary-dark justify-center p-20'>
      <div className='w-96 pr-20  hidden lg:grid '>
        <img src={loginImg} className='m-auto'></img>
      </div>

      <form
        onSubmit={handleSubmit(login)}
        className='flex flex-col gap-2 space-y-2  lg:border-l border-grey-500 lg:pl-20'
      >
        <h3 className='border-b'>Login</h3>
        <div>
          <div className='flex md:flex-row flex-col gap-2 md:items-center'>
            <label className='w-20' htmlFor='username'>
              Username
            </label>
            <input
              id='username'
              {...register('username')}
              className='input-primary flex-grow'
            />
          </div>
          <ErrorMsg errors={errors?.username} reqErrors={reqError?.username} />
        </div>

        <div>
          <div className='flex md:flex-row flex-col gap-2 md:items-center'>
            <label className='w-20' htmlFor='password'>
              Password
            </label>
            <input
              type='password'
              id='password'
              {...register('password')}
              className='input-primary flex-grow'
            />
          </div>
          <ErrorMsg errors={errors?.password} reqErrors={reqError?.password} />
        </div>
        <div>
          <button type='submit' className='btn-primary w-full h-15'>
            LOGIN
          </button>
        </div>
      </form>
    </div>
  );
}

export default Login;
