import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import PublicPostItem from './component/PublicPostItem';
import { useSearchParams } from 'react-router-dom';
import useDebounce from '../../../hooks/useDebounce';
import { searchPublicPosts } from '../../../service/postService';

Home.propTypes = {};

function Home(props) {
  const [searchParams, setSearchParams] = useSearchParams();
  const [searchValue, setSearchValue] = useState('');
  const debounceSeachValue = useDebounce(searchValue, 1000);
  const [posts, setPosts] = useState([]);

  // excecute search query params after input iddle
  useEffect(() => {
    setSearchParams({ search: debounceSeachValue });
  }, [debounceSeachValue]);

  // excecute data fetching from service according to search params
  useEffect(() => {
    const search = searchParams.get('search');
    if (search) {
      const filterResult = searchPublicPosts(search);
      setPosts(filterResult);
    } else {
      const filterResult = searchPublicPosts('');
      setPosts(filterResult);
    }
  }, [searchParams]);

  return (
    <div className='space-y-5'>
      <h3>Wellcome!</h3>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          setSearchParams({ search: searchValue });
        }}
      >
        {/* search */}
        <input
          value={searchValue}
          onChange={(e) => {
            setSearchValue(e.target.value);
          }}
          className='input-primary w-full h-13 '
          placeholder='Search...'
        />
      </form>

      {/* list of post */}
      <div className='grid grid-cols-1 md:grid-cols-2 gap-5'>
        {posts.map((post, index) => {
          return <PublicPostItem key={post.id} post={post} />;
        })}
      </div>
    </div>
  );
}

export default Home;
