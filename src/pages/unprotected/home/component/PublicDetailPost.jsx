import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import { getPublicPostById } from '../../../../service/postService';

PublicDetailPost.propTypes = {};

function PublicDetailPost(props) {
  const { id } = useParams();
  const [postData, setPostData] = useState({});

  useEffect(() => {
    setPostData(getPublicPostById(id));
  }, [id]);

  return (
    <div className=' w-full  bg-secondary-dark p-10 space-y-5'>
      <h3 className='pb-2 border-b'>{postData.title}</h3>
      <span className='text-sm text-secondary-light'>
        Posted By {postData.writerId} {moment(postData.updatedAt).fromNow()}
      </span>
      <p>{postData.content}</p>
    </div>
  );
}

export default PublicDetailPost;
