import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';

PublicPostItem.propTypes = {};

PublicPostItem.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.string,
  }),
};

function PublicPostItem(props) {
  const {
    post: { id, title, content },
  } = props;
  
  return (
    <div className='p-10 bg-secondary-dark space-y-5  '>
      <Link to={'/post/' + id}>
        <h4 className='border-b text-teal-500 transition-colors duration-300'>
          {title}
        </h4>
      </Link>

      <span className='text-sm text-secondary-light'>
        Updated {moment('2022-06-14T15:15:15.076Z').fromNow()}
      </span>

      <p className='text-white'>
        {content.length > 100 ? (
          <span>
            {content.slice(0, 100) + '...'}
            <Link
              to={'/posts/' + id + '?edit=false'}
              className='underline hover:text-teal-500'
            >
              Read More
            </Link>
          </span>
        ) : (
          content
        )}
      </p>
    </div>
  );
}

export default PublicPostItem;
