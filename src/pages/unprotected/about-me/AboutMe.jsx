import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import {
  faEnvelope,
  faPhone,
  faPrint,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import profilePicture from '../../../assets/images/alidin.png';
import CvSidebar from './component/CvSidebar';
import FeaturedProjects from './component/FeaturedProjects';
import Sertificates from './component/Sertificates';
import WorkExperience from './component/WorkExperience';

function AboutMe(props) {
  return (
    <div className='overflow-auto'>
      <div className=' p-10 bg-secondary-dark space-y-5 print:text-primary-dark print:p-30 min-w-220 overflow-x-auto'>
        <div className='flex flex-row justify-between'>
          <div className='flex flex-col border-b-8 border-primary-teal'>
            <h1>Alidin</h1>
          </div>
          <div className='flex flex-col'>
            <span className='inline-flex items-center gap-2'>
              <div className=' text-primary-dark bg-gray-200 rounded w-5 h-5 grid'>
                <FontAwesomeIcon icon={faPhone} className='w-3 m-auto' />
              </div>
              <span>081337884868</span>
            </span>
            <span className='inline-flex items-center gap-2'>
              <div className=' text-primary-dark bg-gray-200 rounded w-5 h-5 grid'>
                <FontAwesomeIcon icon={faEnvelope} className='w-3 m-auto' />
              </div>
              <span>Alidin202@gmail.com</span>
            </span>
            <span className='inline-flex items-center gap-2'>
              <div className=' text-primary-dark bg-gray-200 rounded w-5 h-5 grid'>
                <FontAwesomeIcon icon={faGitlab} className='w-3 m-auto' />
              </div>
              <span>gitlab.com/Udin_lab</span>
            </span>
          </div>
        </div>
        <div className='flex flex-row gap-5 items-center'>
          <div>
            <div className='rounded-full bg-white h-32 w-32 overflow-hidden'>
              <img className='w-full' src={profilePicture}></img>
            </div>
          </div>
          <div>
            I'm a junior full stack developer with about 1.5 years of
            experience, I'm a person who wants to master everything related to
            something I like, especially if it's related to programming,
            critical thinking, and problem solving. I don't know when to give up
            when faced with complex programming cases that require critical
            thinking and accurate problem solving. for this reason, I decided to
            pursue a career in the field of fullstack website development and
            sometimes I'm also working on Machine Learning and Artificial
            Intelligence Project.
          </div>
        </div>

        <hr className='border-gray-500 ' />

        <div className='flex flex-row '>
          <div className='flex flex-col gap-3 border-r border-gray-500 pr-7'>
            {/* <Biodata /> */}
            <WorkExperience />
            <FeaturedProjects />
            <Sertificates />
          </div>
          <div className='min-w-72 flex flex-col pl-7'>
            <CvSidebar />
          </div>
        </div>

        <div className='inline-flex items-center justify-center border-t border-gray-500 pt-2'>
          <a href='https://www.linkedin.com/in/alidin-alidin-33808223a/'>
            <span className='space-x-2 '>
              <FontAwesomeIcon icon={faLinkedin} />
              <span>linkedin.com/in/alidin-alidin-33808223a</span>
            </span>
          </a>
        </div>
      </div>
    </div>
  );
}

export default AboutMe;
