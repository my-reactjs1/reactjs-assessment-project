import React from 'react';
import PropTypes from 'prop-types';

NotFound.propTypes = {};

function NotFound(props) {
  return (
    <div className='w-full h-full text-center grid'>
      <span className='m-auto '>
        <h1 className='text-gray-600' >404</h1>
        <h5 className='text-gray-600' >NOT FOUND</h5>
      </span>
    </div>
  );
}

export default NotFound;
