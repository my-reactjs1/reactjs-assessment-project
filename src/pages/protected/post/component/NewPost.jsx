import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import CheckBox from '../../../../component/form/CheckBox';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import ErrorMsg from '../../../../component/form/ErrorMsg';
import { createNewPost } from '../../../../service/postService';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

const postSchema = yup
  .object()
  .shape({
    writerId: yup.string().required('Writer id is required'),
    title: yup.string().required('Title is Required'),
    content: yup.string().required('Content is Required'),
    isPublic: yup.boolean().required('').default(false),
  })
  .required();

NewPost.propTypes = {};

function NewPost(props) {
  // get user auth data to be used as writer id
  const auth = useSelector((state) => state.auth);

  // initialize form validation with yup shcemas
  const {
    formState: { errors },
    handleSubmit,
    register,
    setValue,
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(postSchema),
  });

  // execute cretae new post
  const navigate = useNavigate();
  function createNew(form, event) {
    const created = createNewPost(form);
    navigate('/posts/' + created.id);
  }

  // initialy set writer id as active user id
  useEffect(() => {
    setValue('writerId', auth.value.id);
  }, [auth]);

  //
  return (
    <form
      onSubmit={handleSubmit(createNew)}
      className='p-10 bg-secondary-dark space-y-5 flex flex-col'
    >
      <h3>New Post</h3>

      {/* title */}
      <div className='flex flex-col w-full gap-2'>
        <label htmlFor='title'>Title</label>
        <input
          {...register('title')}
          type='text'
          id='title'
          className='input-primary w-full'
        />
        <ErrorMsg errors={errors.title} />
      </div>

      {/* content */}
      <div className='flex flex-col w-full gap-2'>
        <label htmlFor='content'>Content</label>
        <textarea
          {...register('content')}
          type='text'
          id='content'
          className='input-primary w-full h-80'
        />
        <ErrorMsg errors={errors.content} />
      </div>

      {/* isPublic */}
      <CheckBox
        onChange={(e) => {
          setValue('isPublic', e);
        }}
      >
        Make Public
      </CheckBox>

      {/* form control */}
      <div className='flex flex-row gap-2 justify-end'>
        <button type='reset' className='w-32 btn-secondary'>
          Cancel
        </button>
        <button type='submit' className='w-32 btn-primary'>
          Create
        </button>
      </div>
    </form>
  );
}

export default NewPost;
