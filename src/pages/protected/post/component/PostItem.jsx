import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import CheckBox from '../../../../component/form/CheckBox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGear, faPencil } from '@fortawesome/free-solid-svg-icons';
import { togglePublicOfPostById } from '../../../../service/postService';
import { useSelector } from 'react-redux';

PostItem.propTypes = {
  post: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    content: PropTypes.string,
    isPublic: PropTypes.bool,
  }),
};

function PostItem(props) {
  const auth = useSelector((state) => state.auth);
  const {
    post: { id, title, content, isPublic },
  } = props;

  return (
    <div className='p-10 bg-secondary-dark space-y-5  '>
      <div className='flex flex-row justify-between items-start w-full'>
        <Link to={id + '?edit=false'}>
          <h4 className='border-b text-teal-500 transition-colors duration-300'>
            {title}
          </h4>
        </Link>

        <Link to={id + '?edit=true'}>
          <FontAwesomeIcon
            icon={faPencil}
            className='h-5 w-5 text-teal-500 hover:text-teal-700 transition-colors '
          />
        </Link>
      </div>

      <span className='text-sm text-secondary-light'>
        Updated {moment('2022-06-14T15:15:15.076Z').fromNow()}
      </span>

      <p className='text-white'>
        {content.length > 100 ? (
          <span>
            {content.slice(0, 100) + '...'}
            <Link
              to={'/posts/' + id + '?edit=false'}
              className='underline hover:text-teal-500'
            >
              Read More
            </Link>
          </span>
        ) : (
          content
        )}
      </p>

      <CheckBox
        id={id}
        checked={isPublic}
        onChange={() => {
          togglePublicOfPostById(auth.value.id, id);
        }}
      >
        <span className='text-secondary-light hover:text-teal-500'>
          Make Public
        </span>
      </CheckBox>
    </div>
  );
}

export default PostItem;
