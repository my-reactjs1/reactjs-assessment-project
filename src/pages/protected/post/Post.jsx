import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useSearchParams } from 'react-router-dom';
import useDebounce from '../../../hooks/useDebounce';
import { searchUserPosts } from '../../../service/postService';
import PostItem from './component/PostItem';

Post.propTypes = {};

function Post(props) {
  const auth = useSelector((state) => state.auth);
  const [posts, setPosts] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [searchParams, setSearchParams] = useSearchParams();
  const debounceSeachValue = useDebounce(searchValue, 1000);

  // debounce effect
  useEffect(() => {
    setSearchParams({ search: debounceSeachValue });
  }, [debounceSeachValue]);

  // excecute data searching based on the search param
  useEffect(() => {
    const search = searchParams.get('search');
    if (search) {
      const filterResult = searchUserPosts(auth.value?.id, search);
      setPosts(filterResult);
    } else {
      const filterResult = searchUserPosts(auth.value?.id, '');
      setPosts(filterResult);
    }
  }, [searchParams, auth]);

  return (
    <div className='space-y-5'>
      <h3 className='border-b'>My Posts</h3>
      <div className='flex lg:flex-row flex-col justify-between gap-2 lg:items-center '>
        <div className='inline-flex items-center flex-grow '>
          <input
            value={searchValue}
            onChange={(e) => {
              setSearchValue(e.target.value);
            }}
            className='input-primary flex-grow md:flex-grow-0'
            placeholder='Search...'
          />
        </div>

        <Link to={'/posts/new'} className='flex md:flex-row justify-end'>
          <button className='btn-primary inline-flex gap-2 items-center'>
            <FontAwesomeIcon icon={faPlus} />
            Create New Post
          </button>
        </Link>
      </div>
      <div className='grid grid-cols-1 lg:grid-cols-2 gap-5'>
        {posts.map((post, index) => {
          return <PostItem key={post.id} post={post} />;
        })}
      </div>
    </div>
  );
}

export default Post;
