import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import CheckBox from '../../../../component/form/CheckBox';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import ErrorMsg from '../../../../component/form/ErrorMsg';
import { useNavigate, useParams, useSearchParams } from 'react-router-dom';
import {
  deletePostById,
  getCurrenUserPostById,
  togglePublicOfPostById,
  updateCurrenUserPostById,
} from '../../../../service/postService';
import { useSelector } from 'react-redux';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencil } from '@fortawesome/free-solid-svg-icons';
import OkCancelWarning from '../../../../component/modal/OkCancelWarning';

const postSchema = yup
  .object()
  .shape({
    title: yup.string().required('Title is Required'),
    content: yup.string().required('Content is Required'),
    isPublic: yup.boolean().required('').default(false),
  })
  .required();

DetailPost.propTypes = {};

function DetailPost(props) {
  const { id } = useParams();

  const auth = useSelector((state) => state.auth);
  const navigate = useNavigate();

  const [postData, setPostData] = useState({});
  const [isDeleting, setIsDeleting] = useState(false);

  const [searchParams, setSearchParams] = useSearchParams();
  const editMode = searchParams.get('edit');

  const {
    formState: { errors },
    handleSubmit,
    register,
    reset,
    setValue,
    getValues,
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(postSchema),
  });

  // refetch the data after update/changing route
  function refetch() {
    const data = getCurrenUserPostById(auth.value?.id, id);
    if (!data) {
      navigate('/404');
    }
    setPostData(data);
    reset(data);
  }

  // refetching data
  useEffect(() => {
    refetch();
  }, [id]);

  // update form handler
  function handleUpdate(formData, e) {
    updateCurrenUserPostById(auth.value.id, id, formData);
    refetch();
    setSearchParams({ edit: false });
  }

  // handle data deletion
  function handleDeletion() {
    deletePostById(auth.value.id, id);
    navigate('/posts');
  }

  return editMode === 'true' ? (
    <form
      onSubmit={handleSubmit(handleUpdate)}
      className='p-10 bg-secondary-dark space-y-5 flex flex-col'
    >
      <h3>Update Post</h3>
      <div className='flex flex-col w-full gap-2'>
        <label htmlFor='title'>Title</label>
        <input
          {...register('title')}
          type='text'
          id='title'
          className='input-primary w-full'
        />
        <ErrorMsg errors={errors.title} />
      </div>

      {/* content */}
      <div className='flex flex-col w-full gap-2'>
        <label htmlFor='content'>Content</label>
        <textarea
          {...register('content')}
          type='text'
          id='content'
          className='input-primary w-full h-80'
        />
        <ErrorMsg errors={errors.content} />
      </div>

      {/* isPublic */}
      <CheckBox
        id={id}
        checked={getValues('isPublic')}
        onChange={(e) => {
          setValue('isPublic', e);
        }}
      >
        Make Public
      </CheckBox>
      <div className='flex flex-row gap-2 justify-end'>
        <button
          type='button'
          onClick={(e) => {
            setIsDeleting(!isDeleting);
          }}
          className='w-32 btn-danger'
        >
          Delete
        </button>
        <button
          onClick={(e) => {
            reset(postData);
            setSearchParams({ edit: false });
          }}
          type='button'
          className='w-32 btn-secondary'
        >
          Cancel
        </button>
        <button type='submit' className='w-32 btn-primary'>
          Save
        </button>
      </div>

      {/**
       * DELETION MODAL
       * */}
      <OkCancelWarning
        visible={isDeleting}
        title='Confirm deletion'
        setVisible={setIsDeleting}
        onDelete={handleDeletion}
      >
        Are you sure want to delete this post?, this action cannot be undone!
      </OkCancelWarning>
    </form>
  ) : (
    <div className=' w-full  bg-secondary-dark p-10 space-y-5'>
      <div className='flex flex-row justify-between'>
        <h3 className='pb-2 border-b'>{postData.title}</h3>
        <button
          onClick={(e) => {
            setSearchParams({ edit: true });
          }}
        >
          <FontAwesomeIcon
            icon={faPencil}
            className='h-5 w-5 text-teal-500 hover:text-teal-700 transition-colors '
          />
        </button>
      </div>
      <span className='text-sm text-secondary-light'>
        Posted {moment(postData.updatedAt).fromNow()}
      </span>
      <p>{postData.content}</p>
    </div>
  );
}

export default DetailPost;
