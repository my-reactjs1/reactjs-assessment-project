import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

// hooks for easy shorctcut keyboard access
function useKey(key, cb) {
  const callback = useRef(cb);

  useEffect(() => {
    callback.current = cb;
  });

  useEffect(() => {
    function handle(event) {
      if (event.code === key) {
        callback.current(event);
      }
    }

    document.addEventListener('keydown', handle);
    return () => document.removeEventListener('keydown', handle);
  }, [key]);
}

export default useKey;
